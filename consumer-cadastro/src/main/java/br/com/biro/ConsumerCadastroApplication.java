package br.com.biro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumerCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsumerCadastroApplication.class, args);
	}

}
