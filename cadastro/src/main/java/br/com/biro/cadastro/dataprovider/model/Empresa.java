package br.com.biro.cadastro.dataprovider.model;

import javax.persistence.*;

@Entity
@Table(name = "empresa")
public class Empresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "cnpj", nullable = false, length = )
    private String cnpj;
}
