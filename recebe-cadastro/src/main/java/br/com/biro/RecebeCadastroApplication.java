package br.com.biro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RecebeCadastroApplication {

	public static void main(String[] args) {
		SpringApplication.run(RecebeCadastroApplication.class, args);
	}

}
